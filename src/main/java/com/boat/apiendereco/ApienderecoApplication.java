package com.boat.apiendereco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApienderecoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApienderecoApplication.class, args);
	}

}
