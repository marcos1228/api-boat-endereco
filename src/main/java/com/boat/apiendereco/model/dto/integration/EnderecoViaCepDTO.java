package com.boat.apiendereco.model.dto.integration;

import lombok.Data;

@Data
public class EnderecoViaCepDTO {
	private String cep;
	private String logradouro;
	private String complemento;
	private String bairro;
	private String localidade;
	private String uf;
	private String ddd;
}
